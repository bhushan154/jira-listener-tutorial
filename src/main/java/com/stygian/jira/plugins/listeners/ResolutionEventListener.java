package com.stygian.jira.plugins.listeners;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.Issue;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * Created with IntelliJ IDEA.
 * User: bhushan154
 * Date: 08/07/13
 * Time: 8:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class ResolutionEventListener implements InitializingBean, DisposableBean {

    private EventPublisher eventPublisher = null;

    public ResolutionEventListener(EventPublisher eventPublisher) {
         this.eventPublisher = eventPublisher;
    }

    /**
     * Called when the plugin has been enabled.
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        // register ourselves with the EventPublisher
        eventPublisher.register(this);
    }

    /**
     * Called when the plugin is being disabled or removed.
     * @throws Exception
     */
    @Override
    public void destroy() throws Exception {
        // unregister ourselves with the EventPublisher
        eventPublisher.unregister(this);
    }

    @EventListener
    public void onIssueEvent(IssueEvent issueEvent) {
        Long eventTypeId = issueEvent.getEventTypeId();
        Issue issue = issueEvent.getIssue();
        if (eventTypeId.equals(EventType.ISSUE_RESOLVED_ID)) {
            //Perform your logic here
        }
    }
}
